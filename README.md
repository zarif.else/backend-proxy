# Myhealth app API Proxy

NGINX proxy app for the MyHealth app API

## Usage

### Environment variables

- `LISTEN_PORT` - Port to listen on (default: `8000`)
- `APP_HOST` - Hostname of the app tio forward requests to (default: `app`)
- `APP_PORT` - Port of the app to forwared requests to (default: `9000`)

### GITLab Flow
